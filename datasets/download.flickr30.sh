#!/bin/bash

# PLEASE fill the form in https://illinois.edu/fb/sec/229675
#
# URLS get after filling the form, http://shannon.cs.illinois.edu/DenotationGraph/data/index.html


mkdir -P Flickr30kEntities

wget http://shannon.cs.illinois.edu/DenotationGraph/data/flickr30k-images.tar -P Flickr30kEntities/flickr30k_images
wget http://shannon.cs.illinois.edu/DenotationGraph/data/flickr30k.tar.gz -P Flickr30kEntities

tar xvzf Flickr30kEntities/flickr30k.tar.gz
tar xvf Flickr30kEntities/flickr30k-images.tar
