#!bin/bash

mkdir -P coco

wget http://msvocds.blob.core.windows.net/annotations-1-0-3/captions_train-val2014.zip -P coco
wget http://msvocds.blob.core.windows.net/coco2014/train2014.zip -P coco
wget http://msvocds.blob.core.windows.net/coco2014/val2014.zip -P coco


unzip coco/captions_train-val2014.zip
unzip coco/train2014.zip -d coco/images/
unzip data/val2014.zip -d coco/images/

rm coco/train2014.zip 
rm coco/val2014.zip 


