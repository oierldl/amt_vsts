import os
import sys
import argparse
import numpy as np
# from nltk.corpus import stopwords

punctuations = ['(','-lrb-','.',',','-','?','!',';','_',':','{','}','[','/',']','...','"','\'',')', '-rrb-']

def main(arguments):
    '''
    Read STSBenchmark official dataset and produce txt files for sent1, sent2 and similarity score
    '''
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data_file', help="location of sts file")
    parser.add_argument('--out_file', help="location of the output file")
    
    args = parser.parse_args(arguments)
    
    out = open(args.out_file, "w")
    i = 0
    for line in open(args.data_file,"r"):
        if i == 0:
            i += 1
            continue
        d = line.split("\t")
        label = "0" # no label 
        sent1 = d[2].strip().lower()
        sent2 = d[4].strip().lower()
            
        for punct in punctuations:
            if punct in sent1:
                sent1 = sent1.replace(punct,"")
            
        for punct in punctuations:
            if punct in sent2:
                sent2 = sent2.replace(punct,"")

        out.write(sent1 + '\t' + sent2 + '\t' + label + "\n")

    out.close()
           
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
