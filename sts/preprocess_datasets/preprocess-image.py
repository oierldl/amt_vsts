import os
import sys
import argparse
import numpy as np
from os.path import basename

def main(arguments):
    '''

    '''
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data_file', help="location of STS file")
    parser.add_argument('--out_file', help="location of the output folder")
    parser.add_argument('--source', default=None, help="change prefix of embedding ids")
    
    args = parser.parse_args(arguments)
    
    out = open(args.out_file, "w")
    i = 0
    for line in open(args.data_file,"r"):
        if i == 0:
            i += 1
            continue
        d = line.split("\t")
        label = "0"
        image1 = d[3].strip()
        image2 = d[5].strip()

        if args.source is not None:
            image1 = args.source+'/'+basename(image1)
            image2 = args.source+'/'+basename(image2)
        
        out.write(image1 + '\t' + image2 + '\t' + label + "\n")

    out.close()
           
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
