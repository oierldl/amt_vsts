from __future__ import print_function

import sys, os, re, shutil
import argparse
import random
import collections
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats.stats import pearsonr

import sts_utils as utils

# def load_data(path):
#     data = []
#     with open(path) as f:
#         for line in f:
#             example = {}
#             text1, text2, gold = line.rstrip().split("\t")
#             example['label'] = float(gold)
#             example['text1'] = text1
#             example['text2'] = text2

#             data.append(example)
#     #random.seed(2112)
#     #random.shuffle(data)
#     return data

def feature_function(datasets):
    '''Annotates datasets with feature vectors.'''

    # Extract vocabulary
    def tokenize(string):
        return string.split()

    word_counter = collections.Counter()
    for dataset in datasets:
        for example in dataset:
            word_counter.update(utils.tokenize(example['text1']))
            word_counter.update(utils.tokenize(example['text2']))

    thr=0
    vocabulary = set([word for word in word_counter if word_counter[word] > thr])


    def extract_features(text):
        features = collections.defaultdict(float)
        #word_counter = collections.Counter(tokenize(text))
        # tokenize
        tokens = utils.tokenize(text)
        # strip punctuation
        tokens = utils.strip_punctuation(tokens)
        # remove stopwords
        tokens = utils.remove_stopwords(tokens)

        #for x in word_counter.items():
        for x in tokens:
            if x in vocabulary:
                #features["word_count_for_" + x[0]] = x[1]
                features[x] = 1
        return features
        
    feature_names = set()
    for i, dataset in enumerate(datasets):
        for example in dataset:
            # Extract features (by name) for one example
            features = extract_features(example['text1'])
            example['features1'] = features
            feature_names.update(features.keys())
            
            features = extract_features(example['text2'])
            example['features2'] = features
            feature_names.update(features.keys())


    # By now, we know what all the features will be, so we can
    # assign indices to them.
    feature_indices = dict(zip(feature_names, range(len(feature_names))))
    indices_to_features = {v: k for k, v in feature_indices.items()}
    dim = len(feature_indices)
                
    # Now we create actual vectors from those indices.
    for dataset in datasets:
        for example in dataset:
            example['vector1'] = np.zeros((dim))
            example['vector2'] = np.zeros((dim))
            for feature in example['features1']:
                example['vector1'][feature_indices[feature]] = example['features1'][feature]
            for feature in example['features2']:
                example['vector2'][feature_indices[feature]] = example['features2'][feature]

    return indices_to_features, dim


def predictions(dataset):
    n = len(dataset)
    preds = []
    for i in range(n): 
        preds.append(cosine_similarity(np.array([dataset[i]['vector1'],
                                                 dataset[i]['vector2']]))[0,1] * 5)
    return preds

def save_predictions(filename, predictions):
    fo = open(filename, "w")
    for pred in predictions:
        fo.write("{}\n".format(pred))
    fo.close()

def save_results(out_dir, results):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    fo = open(out_dir +"/prediction_overlap.txt", "w")
    for pred in results[0]:
        fo.write("{}\n".format(pred))
    fo.close()
    fo = open(out_dir +"/gold.txt", "w")
    for gold in results[1]:
        fo.write("{}\n".format(gold))
    fo.close()

    fo = open(out_dir +"/cm_overlap.txt", "w")
    fo.write("{}\n".format(results[2]))
    fo.close()
    

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run overlap based baseline on the indicated project's dataset")

    parser.add_argument("--testFile", type=str, default="test.txt",
                        help="Test file (file inside project dir) [default test.txt]")
    parser.add_argument("--outputFile", type=str, default=None,
                        help="Output of the predicted similarity")
    parser.add_argument("--evaluate", action="store_true",
                        help="Use provided scores for evaluating.")

    args = parser.parse_args()

    
    if args.testFile is None:
        parser.print_help()
        parser.exit()
    if args.outputFile is None:
        parser.print_help()
        parser.exit()
                                   
    #load_data
    test_set = utils.load_data(args.testFile)
    indices_to_features, dim = feature_function([test_set])

    test_preds = predictions(test_set)
    save_predictions(args.outputFile, test_preds)
    
    if args.evaluate:
        test_golds = [ test_set[i]['label'] for i in range(len(test_set)) ]
        test_rho = pearsonr(np.array(test_golds), np.array(test_preds))
        print("Test pearson r: {}".format(test_rho))
