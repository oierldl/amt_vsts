from __future__ import print_function

import sys, os, re, shutil
import argparse
import random
import collections
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats.stats import pearsonr

import sts_utils as utils

# def load_data(path):
#     data = []
#     with open(path) as f:
#         for line in f:
#             example = {}
#             text1, text2, gold = line.rstrip().split("\t")
#             example['label'] = float(gold)
#             example['text1'] = text1
#             example['text2'] = text2

#             data.append(example)
# #    random.seed(2112)
# #    random.shuffle(data)
#     return data

# def read_embeddings(embs_f, vocab):
#     embeddings_index = {}
#     f = open(embs_f, "r", errors='surrogateescape')
#     for emb_line in f:
#         emb = emb_line.split(" ")
#         if emb[0] in vocab:
#             embeddings_index[emb[0]] = np.array(emb[1:], dtype=float)
#     emb_size = len(emb[1:])

#     print('Found {} word vectors of size {}'.format(len(embeddings_index), emb_size))
#     return embeddings_index, emb_size
    
#     # embedding_matrix = np.zeros((len(word_index) + 1, emb_size))
#     # for word, i in word_index.items():
#     #     embedding_vector = embeddings_index.get(word)
#     #     if embedding_vector is not None:
#     #         # words not found in embedding index will be all-zeros.
#     #         embedding_matrix[i] = embedding_vector
#     # return embedding_matrix, emb_size
    

# def tokenize(string):
#         return string.split()

def set_vocabulary(datasets, thr=0):
    # Extract vocabulary
    word_counter = collections.Counter()
    for dataset in datasets:
        for example in dataset:
            word_counter.update(utils.tokenize(example['text1']))
            word_counter.update(utils.tokenize(example['text2']))

    vocabulary = set([word for word in word_counter if word_counter[word] > thr])
    return vocabulary


def feature_function(datasets, vocabulary, embs, emb_size):
    '''Annotates datasets with feature vectors.'''

    word2ind = {word:i for i, word in enumerate(list(embs.keys()))}

    def extract_features(text):
        features = collections.defaultdict(float)
        #word_counter = collections.Counter(tokenize(text))
        word_counter = collections.Counter(utils.tokenize(text))

        # tokenize
        tokens = utils.tokenize(text)
        # recase
        tokens = utils.recase(tokens, word2ind)
        # strip punctuation
        tokens = utils.strip_punctuation(tokens)
        # remove stopwords
        tokens = utils.remove_stopwords(tokens)
        
        #for x in word_counter.items():
        for x in tokens:
            if x in vocabulary: # remove oov words
                #features["word_count_for_" + x[0]] = x[1]
                #features[x[0]] = x[1]
                features[x] = 1
        return features
        
    feature_names = set()
    for i, dataset in enumerate(datasets):
        for example in dataset:
            # Extract features (by name) for one example
            features = extract_features(example['text1'])
            example['features1'] = features
            feature_names.update(features.keys())
            
            features = extract_features(example['text2'])
            example['features2'] = features
            feature_names.update(features.keys())


    # By now, we know what all the features will be, so we can
    # assign indices to them.
    feature_indices = dict(zip(feature_names, range(len(feature_names))))
    indices_to_features = {v: k for k, v in feature_indices.items()}
    dim = len(feature_indices)
                
    # Now we create actual vectors from those indices.
    for dataset in datasets:
        for example in dataset:
            seq_length1 = len(example['features1'])
            seq_length2 = len(example['features2'])
            example['tensor1'] = np.zeros((seq_length1, emb_size))
            example['tensor2'] = np.zeros((seq_length2, emb_size))
            
            for i, feature in enumerate(example['features1']):
                if feature in embs:
                    example['tensor1'][i,:] = embs[feature]
            for i, feature in enumerate(example['features2']):
                if feature in embs:
                    example['tensor2'][i,:] = embs[feature]

    return indices_to_features


def predictions(dataset):
    n = len(dataset)
    preds = []
    for i in range(n):
        # get context averages:
        avg1 = np.mean(dataset[i]['tensor1'], 0)
        avg2 = np.mean(dataset[i]['tensor2'], 0)

        # compute similarity
        preds.append(cosine_similarity(np.array([avg1, avg2]))[0,1] * 5)
    return preds

def save_predictions(filename, predictions):
    fo = open(filename, "w")
    for pred in predictions:
        fo.write("{}\n".format(pred))
    fo.close()

def save_results(out_dir, results):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    fo = open(out_dir +"/prediction_caverage.txt", "w")
    for pred in results[0]:
        fo.write("{}\n".format(pred))
    fo.close()
    fo = open(out_dir +"/gold.txt", "w")
    for gold in results[1]:
        fo.write("{}\n".format(gold))
    fo.close()

    fo = open(out_dir +"/cm_caverage.txt", "w")
    fo.write("{}\n".format(results[2]))
    fo.close()
    

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run context embedding-average model")

    parser.add_argument("--testFile", type=str, default="test.txt",
                        help="Test file (file inside project dir) [default test.txt]")

    parser.add_argument("--embs", type=str, default="glove",
                        help="Embedding names [default glove]")
    
    parser.add_argument("--outputFile", type=str, default=None,
                        help="Output of the predicted similarity")
    parser.add_argument("--evaluate", action="store_true",
                        help="Use provided scores for evaluating.")

    
    args = parser.parse_args()

    if args.testFile is None:
        parser.print_help()
        parser.exit()
    if args.embs is None:
        parser.print_help()
        parser.exit()
    if args.outputFile is None:
        parser.print_help()
        parser.exit()

                                 
    #load_data
    test_set = utils.load_data(args.testFile)

    # vocabulary
    vocabulary = set_vocabulary([test_set])
    
    # Read embeddings to be used as features:
    embs, emb_size= utils.read_embeddings(args.embs, vocabulary)
    
    indices_to_features = feature_function([test_set],
                                           vocabulary, embs, emb_size )

    test_preds = predictions(test_set)
    save_predictions(args.outputFile, test_preds)
    
    if args.evaluate:
        test_golds = [ test_set[i]['label'] for i in range(len(test_set)) ]
        test_rho = pearsonr(np.array(test_golds), np.array(test_preds))
        print("Test pearson r: {}".format(test_rho))

