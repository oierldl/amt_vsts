import numpy as np
import nltk
import nltk.corpus
import nltk.stem
import string

stopwords = set(nltk.corpus.stopwords.words('english'))
stemmer = nltk.stem.SnowballStemmer('english')


def load_data(path):
    data = []
    with open(path) as f:
        for line in f:
            example = {}
            text1, text2, gold = line.rstrip().split("\t")
            example['label'] = float(gold)
            example['text1'] = text1
            example['text2'] = text2

            data.append(example)
#    random.seed(2112)
#    random.shuffle(data)
    return data


def read_embeddings(embs_f, vocab):
    embeddings_index = {}
    #f = open(embs_f, "r", errors='surrogateescape')
    f = open(embs_f, "r")
    for emb_line in f:
        emb = emb_line.split(" ")
        if emb[0] in vocab:
            embeddings_index[emb[0]] = np.array(emb[1:], dtype=float)
    emb_size = len(emb[1:])

    print('Found {} word vectors of size {}'.format(len(embeddings_index), emb_size))
    return embeddings_index, emb_size
    
    # embedding_matrix = np.zeros((len(word_index) + 1, emb_size))
    # for word, i in word_index.items():
    #     embedding_vector = embeddings_index.get(word)
    #     if embedding_vector is not None:
    #         # words not found in embedding index will be all-zeros.
    #         embedding_matrix[i] = embedding_vector
    # return embedding_matrix, emb_size
    

def tokenize(string):
    return nltk.word_tokenize(string)
    #return string.split()


def recase(sent, word2ind):
    ans = []
    for word in sent:
        if word in word2ind and word.lower() in word2ind:
            if word2ind[word.lower()] < word2ind[word]:
                ans.append(word.lower())
            else:
                ans.append(word)
        elif word.lower() in word2ind:
            ans.append(word.lower())
        else:
            ans.append(word)
    return ans


def strip_punctuation(sentence):
    return [word for word in sentence if not all([c in string.punctuation for c in word])]

def remove_stopwords(sentence):
    return [word for word in sentence if word not in stopwords]


