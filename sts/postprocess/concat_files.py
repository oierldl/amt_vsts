import argparse
import pandas as pd


def load_tsv(fname):
    df = pd.read_csv(fname, sep="\t")
    return df


def write_data(df, fname=None):
    df.to_csv(fname, sep='\t', index=False)


def main():
    parser = argparse.ArgumentParser(description="Merges two tsv files\n")
    parser.add_argument('file1', type=str, help="Input file 1")
    parser.add_argument('file2', type=str, help="Input file 2")
    parser.add_argument('ofile', type=str, help="Output file")

    data1 = load_tsv(args.file1)
    data2 = load_tsv(args.file2)
    conc = pd.concat([data1, data2])
    write_data(conc, args.ofile)


if __name__ == "__main__":
    main()
