import argparse
import re
import pandas as pd


def read_conf(filename):
    conf = {}
    f = open(filename, 'r')
    for line in f:
        if re.search('^#', line):
            continue
        if re.search('^\n', line):
            continue

        key, value = re.split(' *= *', line.rstrip())
        conf[key] = value
    f.close()
    return conf
            

def read_predictions(filename, name):
    preds = pd.read_table(filename, header=None)
    preds.columns=[name]
    return preds


def read_examples(filename):
    return pd.read_csv(filename, sep='\t')


def join_predictions(conf):
    sys_names = re.split(' *, *', conf['systems'])
    sts_pairs = read_examples(conf['data_source'])

    for name in sys_names:
        preds = read_predictions(conf[name], name)
        sts_pairs = pd.concat([sts_pairs, preds], axis=1)
    return sts_pairs


def write(df, filename):
    df.to_csv(filename, sep='\t', index=False, header=True)


def main():
    parser = argparse.ArgumentParser(description='Retrieve and merge predictions into same data table')
    parser.add_argument('--config_file', default='./config.file', help='Path to configure file')
    args=parser.parse_args()
    
    conf = read_conf(args.config_file)
    
    sts_data = join_predictions(conf)
    write(sts_data, conf['output'])
    
    
if __name__ == "__main__":
    main()
        
                              
