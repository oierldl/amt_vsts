#!/bin/bash

# Set this parameters as required
TMP=STS_coco
BOW=.

echo "Running caverage model on randomly sampled pairs"
python "$BOW/BOW/runSTS_caverage.py" \
	--testFile "$TMP/bow/data.tsv" \
	--outputFile "$TMP/bow/prediction.caverage.txt" \
	--embs "$TMP/embeddings/glove.txt"

echo "Running caverage model on image similarity driven sampling pairs"
python "$BOW/BOW/runSTS_caverage.py" \
	--testFile "$TMP/bow/data.img-sim.tsv" \
	--outputFile "$TMP/bow/prediction.caverage.img-sim.txt" \
	--embs "$TMP/embeddings/glove.txt"
