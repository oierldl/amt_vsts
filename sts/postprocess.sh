#!/bin/bash


# join STS_coco predictions
python postprocess/join_predictions.py --config_file postprocess/coco.config.file
python postprocess/join_predictions.py --config_file postprocess/coco.img-sim.config.file

# join STS_flickr30k predictions
python postprocess/join_predictions.py --config_file postprocess/flickr30k.config.file
python postprocess/join_predictions.py --config_file postprocess/flickr30k.img-sim.config.file


# merge sources of random sampling
file1=STS_flickr30k/flickr30k.pairs.token.preds.tsv
file2=STS_coco/coco.pairs.token.preds.tsv
outfile=../datasets/flickr30k+coco.pairs.token.preds.tsv
python concat_files.py $file1 $file2 $outfile

# merge sources of image-similarity sampling
file1=STS_flickr30k/flickr30k.pairs.img-sim.token.preds.tsv
file2=STS_coco/coco.pairs.img-sim.token.preds.tsv
outfile=../datasets/flickr30k+coco.pairs.img-sim.token.preds.tsv
python concat_files.py $file1 $file2 $outfile
