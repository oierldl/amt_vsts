from __future__ import print_function

import sys, os, re, shutil
import argparse
import random
import collections
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats.stats import pearsonr


def load_image_ids(path, skip_first=False):
    data = []
    with open(path) as f:
        if skip_first:
            f.readline()  ## image1 image2 sim
        for line in f:
            example = {}
            image1 = line.rstrip().split("\t")[0]
            image2 = line.rstrip().split("\t")[1]
            gold = line.rstrip().split("\t")[2]
            
            example['label'] = float(gold)
            example['image1'] = image1
            example['image2'] = image2

            data.append(example)
#    random.seed(2112)
#    random.shuffle(data)
    return data

def read_image_embeddings(embs_files):
    embeddings_index = {}
    for  embs_f in embs_files:
        f = open(embs_f, "r")
        for emb_line in f:
            emb = emb_line.split(" ")
            embeddings_index[emb[0]] = np.array(emb[1:], dtype=float)
    emb_size = len(emb[1:])

    print('Found {} vectors of size {}'.format(len(embeddings_index), emb_size))
    return embeddings_index, emb_size
        

def predictions(dataset, embs):
    n = len(dataset)
    preds = []
    for i in range(n):
        # compute similarity
        preds.append(cosine_similarity(np.array([ embs[ dataset[i]['image1'] ],
                                                  embs[ dataset[i]['image2'] ] ] ))[0,1] * 5)
    return preds

def save_predictions(filename, predictions):
    fo = open(filename, "w")
    for pred in predictions:
        fo.write("{}\n".format(pred))
    fo.close()
    
def save_results(out_dir, results):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    fo = open(out_dir +"/prediction_image.txt", "w")
    for pred in results[0]:
        fo.write("{}\n".format(pred))
    fo.close()
    fo = open(out_dir +"/gold.txt", "w")
    for gold in results[1]:
        fo.write("{}\n".format(gold))
    fo.close()

    fo = open(out_dir +"/cm_image.txt", "w")
    fo.write("{}\n".format(results[2]))
    fo.close()
    

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run context embedding-average model")

    parser.add_argument("--testFile", type=str, default=None,
                        help="Test file for predicting similarity of given two images")
    parser.add_argument("--outputFile", type=str, default=None,
                        help="Output of the predicted similarity")
    parser.add_argument("--embs", type=str, default=None,
                        help="Embedding of images")
    parser.add_argument("--evaluate", action="store_true",
                        help="Use provided scores for evaluating.")


    args = parser.parse_args()

    if args.testFile is None:
        parser.print_help()
        parser.exit()
    if args.embs is None:
        parser.print_help()
        parser.exit()
    if args.outputFile is None:
        parser.print_help()
        parser.exit()

    # Read test image pairs 
    test_set = load_image_ids(args.testFile)
    
    # Read embeddings to be used as features:
    embs, emb_size=read_image_embeddings([args.embs])

    test_preds = predictions(test_set, embs)

    save_predictions(args.outputFile, test_preds)
    
    if args.evaluate:
        test_golds = [ test_set[i]['label'] for i in range(len(test_set)) ]
        test_rho = pearsonr(np.array(test_golds), np.array(test_preds))
        print("Test pearson r: {}".format(test_rho))
    
