import sts_utils as utils
import sts_ml as ml

import argparse
import numpy as np
from sklearn.externals import joblib


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Deploy simple STS ML system on input data')
    parser.add_argument('data', help='test data')
    parser.add_argument('model_path', default='./models', help='path to stored the models')
    parser.add_argument('embeddings', help='the word embeddings')
    parser.add_argument('--model_names', default='model_rf,model_gb,model_kr,model_et', help='name of the models, comma separated (no spaces!)')
    parser.add_argument('--encoding', default='utf-8', help='the character encoding for input (defaults to utf-8)')
    parser.add_argument('--seed', default=0, type=int, help='random seed')
    parser.add_argument('--output_file', default='./predicitions.txt', help='file to store the output of the system')
    parser.add_argument('--eval', help='use annoations to score performance', default=False, action='store_true')
    args = parser.parse_args()

    # Read embeddings
    f = open(args.embeddings, encoding=args.encoding, errors='surrogateescape')
    words, emb = utils.read_embeddings(f)

    # Build word to index map
    word2ind = {word: i for i, word in enumerate(words)}

    # Read data
    test_src, test_trg, test_ref = utils.read_data(open(args.data, encoding=args.encoding, errors='surrogateescape'))

    # Extract features
    test_features1 = ml.extract_features(test_src, test_trg, emb, word2ind)
    test_features2 = ml.extract_features(test_trg, test_src, emb, word2ind)

    # Load models
    models = []
    names = args.model_names.split(',')
    for model in names:
        models.append(joblib.load(args.model_path+'/'+model+'.pkl'))
                
    # Get predictions
    test_sys = sum([m.predict(test_features1) + m.predict(test_features2) for m in models])
    if args.eval:
        print(utils.pearson(test_sys, test_ref))
    
    # Store predictions
    fo = open(args.output_file, 'w')
    for score in test_sys:
        fo.write(str(score / (2*len(names))) + '\n') # scale to 0-5
    fo.close()

if __name__ == '__main__':
    main()
