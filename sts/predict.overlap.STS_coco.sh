#!/bin/bash

# Set this parameters as required
TMP=STS_coco
BOW=.

echo "Running caverage model on randomly sampled pairs"
python "$BOW/BOW/runSTS_overlap.py" \
	--testFile "$TMP/bow/data.tsv" \
	--outputFile "$TMP/bow/prediction.overlap.txt"

echo "Running caverage model on image similarity driven sampling pairs"
python "$BOW/BOW/runSTS_overlap.py" \
	--testFile "$TMP/bow/data.img-sim..tsv" \
	--outputFile "$TMP/bow/prediction.overlap.img-sim.txt"
