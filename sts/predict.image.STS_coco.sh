#!/bin/bash

# Set this parameters as required
TMP=STS_coco
IMAGE=.

echo "Running caverage model on randomly sampled pairs"
python "$IMAGE/Image/runSTS_image.py" \
	--testFile "$TMP/image/data.tsv" \
	--outputFile "$TMP/image/prediction.resnet50.txt" \
	--embs "$TMP/embeddings/coco.resnet50.tsv"

echo "Running caverage model on randomly sampled pairs"
python "$IMAGE/Image/runSTS_image.py" \
	--testFile "$TMP/image/data.img-sim.tsv" \
	--outputFile "$TMP/image/prediction.resnet50.img-sim.txt" \
	--embs "$TMP/embeddings/coco.resnet50.tsv"
