#!/bin/bash

TMP=STS_flickr30k
ML=.
model_path=$ML/models/stsbenchmark-no_images

echo "Running ML model on randomly sampled pairs"
python3 "$ML/ML/predict_ml.py" \
	"$TMP/ml/data.txt" \
	$model_path \
	"$TMP/embeddings/fasttext.txt" \
	--output_file "$TMP/ml/prediction.ml.txt"

echo "Running ML model on image similarity driven sampling pairs"
python3 "$ML/ML/predict_ml.py" \
	"$TMP/ml/data.img-sim.txt" \
	$model_path \
	"$TMP/embeddings/fasttext.txt" \
	--output_file "$TMP/ml/prediction.ml.img-sim.txt" 


	
