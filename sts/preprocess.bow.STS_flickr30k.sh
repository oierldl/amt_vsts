#!/bin/bash

TMP=STS_flickr30k
BOW=.

# create output dirs
mkdir -p $TMP
mkdir -p "$TMP/bow"

# link embedding folder
if [ ! -d $BOW/../embeddings ]; then
    ln -s $BOW/../embeddings $TMP/embeddings
fi


### Random sampling ###
SOURCE_FILE="flickr30k.pairs.token.tsv"
OUTPUT_FILE="data.tsv"

python3 "$BOW/preprocess_datasets/preprocess-bow.py" \
	--data_file "$TMP/source/${SOURCE_FILE}" \
	--out_file "$TMP/bow/${$OUTPUT_FILE}"

### Image similarity driven sampling ###
SOURCE_FILE="flickr30k.pairs.img-sim.token.tsv"
OUTPUT_FILE="data.img-sim.tsv"

python3 "$BOW/preprocess_datasets/preprocess-bow.py" \
	--data_file "$TMP/source/${SOURCE_FILE}" \
	--out_file "$TMP/bow/${$OUTPUT_FILE}"
