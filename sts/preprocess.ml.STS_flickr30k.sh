#!/bin/bash

TMP=STS_flickr30k
ML=.

# create output dirs
mkdir -p $TMP
mkdir -p "$TMP/ml"

# link embedding folder
if [ ! -d $ML/../embeddings ]; then
    ln -s $ML/../embeddings $TMP/embeddings
fi


### Random sampling ###
SOURCE_FILE="flickr30k.pairs.token.tsv"
OUTPUT_FILE="data.tsv"

python3 "$ML/preprocess_datasets/preprocess-ml.py" \
	--data_folder "$TMP/source/${SOURCE_FILE}" \
	--out_folder "$TMP/ml/${OUTPUT_FILE}"


### Image similarity driven sampling ###
SOURCE_FILE="flickr30k.pairs.img-sim.token.tsv"
OUTPUT_FILE="data.img-sim.tsv"

python3 "$ML/preprocess_datasets/preprocess-ml.py" \
	--data_folder "$TMP/source/${SOURCE_FILE}" \
	--out_folder "$TMP/ml/${OUTPUT_FILE}"
