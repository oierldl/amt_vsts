#!/bin/bash

TMP=STS_flickr30k
IMAGE=.

# create output dirs
mkdir -p $TMP
mkdir -p "$TMP/image"

# link embedding folder
if [ ! -d $IMAGE/../embeddings ]; then
    ln -s $IMAGE/../embeddings $TMP/embeddings
fi


### Random sampling ###
SOURCE_FILE="flickr30k.pairs.token.tsv"
OUTPUT_FILE="data.tsv"

python3 "$IMAGE/preprocess_datasets/preprocess-image.py" \
	--data_file "$TMP/source/${SOURCE_FILE}" \
	--out_file "$TMP/image/${OUTPUT_FILE}" \
	--source "flickr30k"

### Image similarity driven sampling ###
SOURCE_FILE="flickr30k.pairs.img-sim.token.tsv"
OUTPUT_FILE="data.img-sim.tsv"

python3 "$IMAGE/preprocess_datasets/preprocess-image.py" \
	--data_file "$TMP/source/${SOURCE_FILE}" \
	--out_file "$TMP/image/${OUTPUT_FILE}" \
	--source "flickr30k"
