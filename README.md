# AMT vSTS: Scripts to generate a dataset for Visual Semantic Text Similarity

This repository contains the scripts that were used to sample pairs
of image-caption tuples. As described below the we need to perfoms various
steps as there is no simple way to generate this type of datasets.


Visual Semantic Textual Similarity (vSTS) is a dataset that allows
studying whether bettersentence representations can be built when having
access to corresponding images, e.g.  a caption and its image,
in contrast with having access to the text alone.

The main goals of this project are two:

- Extend the previous dataset (described [here](http://ixa.si.ehu.es/sites/default/files/dokumentuak/11359/vSTS_clvl2017.camera.pdf)) that only contained about 850 annotated examples.
- Provide tools to sample a balanced dataset, as previous was biased towards lower range of values.


## Generation of dataset ready for AMT

This section describes the creation of the extension of Visual STS
dataset that is going to manually annotate in AMT. In order to create
the datase we make use of two image-caption sources:

- __Flickr30K__: A image-caption dataset that consist about 30K images
in which for each one 5 captions were generated manually.
- __MS-coco__: A image-caption dataset with more than 120K images and 5
captions per images.


In summary we performed in the following steps:

- Generate pairs of captions with the corresponding images.
- Run STS systems on generated pairs Calculate
- Sample balanced dataset for manual annotation


### Generation of pairs

We sampled pair of images images in two different way. In the first one,
we generate pairs by sampling the images randomly. This way, we ensure
high variety when pairing scenes. On the other hand, we pair images
taking in to account the similarity of the images, so we have related
scenes with a higher similarity rate.

----

__Place source files__ (_coco_ and _flickr30k_) in ```datasets``` folder. You can run the following scripts to 
download the captions and images of __MS-COCO__ and __Flickr30k__.
```
   [shell]% cd amt_vsts/datasets
   [shell]% ./download.coco.sh
   [shell]% ./download.flickr30k.sh
```

Alternativelly, you can download manyally __MS-COCO__ from the following urls:

- Download captions from: [http://msvocds.blob.core.windows.net/annotations-1-0-3/captions_train-val2014.zip](http://msvocds.blob.core.windows.net/annotations-1-0-3/captions_train-val2014.zip)
- Download train images from: [http://msvocds.blob.core.windows.net/coco2014/train2014.zip](http://msvocds.blob.core.windows.net/coco2014/train2014.zip)
- Download validation images from: [http://msvocds.blob.core.windows.net/coco2014/val2014.zip](http://msvocds.blob.core.windows.net/coco2014/val2014.zip)

Copy decompressed files to ```datasets/coco/```.

In the case of __Flickr30k__, you can download manually download it from:
   
- [http://bryanplummer.com/Flickr30kEntities/](http://bryanplummer.com/Flickr30kEntities/)

More specifically, captions in the expected format can be downloaded from here:

- [https://github.com/jazzsaxmafia/show_attend_and_tell/tree/master/data/flickr30k/](https://github.com/jazzsaxmafia/show_attend_and_tell/tree/master/data/flickr30k/)

Copy ```results_20130124.token``` to ```datasets/flickr30k/``` and copy the images to ```flickr30k_images```

Source files might change so in that chase you'll need to adapt the scripts. Our scripts expect the following format:

> Each line represents a caption, and consists of a caption ID and the actual caption.  Caption
  IDs are \<image file name\>\#\<caption number\>.  Any captions whose IDs
  share an image file name describe the same image.  As far as we are
  aware, all of the images are distinct.

-----

__Preprocessing__ of MS-Coco to get same format of flickr30k (detailed above):

```
   [shell]% cd sampling   
   [shell]% ./preprocess_coco.sh
```
   
The script produces the ```datasets/annotations/coco_caption.token.tsv```

-----
__Image Embeddings___. We use a pretrained Resnet50 model from Keras to extract the embedding representation of the images in MS-Coco and Flickr30k:

```
   [shell]% cd embeddings
   [shell]% ./coco_embeddings.sh
   [shell]% ./flickr30k_embeddings.sh
```

As a result the scripts produce two embeddings files ```coco.resnet50.tsv``` and ```flickr30k.resnet50.tsv```.


__Word Embeddings___. Depending on the STS model different pre-trained are used to compute the similarity. We make use of Glove and Fasttext embeddings. You need to download and place in the rigth folder (e.g. ```embeddings```). You can do it by running the following scripts:

```
   [shell]% cd embeddings
   [shell]% ./glove_embeddings.sh
   [shell]% ./fasttext_embeddings.sh
```

-----

__Random sampling__. Pairs were created randomly, in which each image is selected once. This way, first we select the first image in the list and match randomly with another image of the remaining pool of images. Once we remove the selected images from the pool, we complete the same procedure for the next image in the list. This way we ensure that each image is paired once and avoid strong bias towards some type of images and similarity types. We randomly picked one caption out of the five available.

Run the following scripts to generate pairs randomly.

```
   [shell]% cd sampling   
   [shell]% ./generate_pairs_flickr30k_rnd.sh
   [shell]% ./generate_pairs_coco_rnd.sh
```

   This will produce two separate files (one for each source),```datasets/coco/coco.pairs.token.tsv``` and ```datasets/flickr30kEntiies/flickr30k.pairs.token.tsv```.

__Image similarity driven sampling__: Pairs were created taking into account the similarity among images. Each image is selected once. We first pick up the first image in the list and match with the most similar image available. Similarity is calculate with the cosine similarity of the pretrained resnest50 image-embedding. Once we remove the selected images from the pool, we complete the same procedure for the next image in the list. This way we ensure that each image is paired once and avoid strong bias towards some type of images and similarities We randomly picked one caption out of the five available.

Run the following scripts to generate pairs randomly.

```
   [shell]% cd sampling   
   [shell]% ./generate_pairs_flickr30k_img-sim.sh
   [shell]% ./generate_pairs_coco_img-sim.sh
```

   This will produce two separate files (one for each source), ```datasets/coco/coco.pairs.token.img-sim.tsv``` and ```datasets/flickr30kEntiies/flickr30k.pairs.token.img-sim.tsv```.

----

### Automatic Similarity Annotation

We provide some STS model to get similarity predictions:

- __Overlap__: BoW model of one-hot encoding with cosine similarity.
- __Caverage__: Glove word embedding based centroid with cosine similarity
- __Image__: Top layer of a pretrained resnet-50 (1000-imagenet) with cosine similarity
- __ML__: Ensemble of feature based machine learning system that make use of a large variety of distance/mt features.

For the final sampling we based on ML model, which we tested it is the most accurate model among provided ones.

We need to make the same steps for all the systems, so we will show the
case for the ML model only.

__Preprocess__ input data. We extract the required information and convert
to a format specified by the STS system. Thus, to prepare Flickr30k pairs for
the ML model, you need to run the next commands.

```
   [shell]% cd sts   
   [shell]% ./preprocess.ml.STS_flickr30k.sh
```

You need to change hard-coded paths in the script to preprocess the pairs sampled according to each heuristic (explained above).

__Predict__ similarities. Similarly to the preprocessing, we prepare a script that make the call to the specific STS model and dataset.

```
   [shell]% cd sts 
   [shell]% ./predict.ml.STS_flickr30k.sh
```

   You need to change hard-coded paths in the script to predict desired input file.
   The output creates a file with predicted similarities (one prediction per line).
   For example, current script will create ```sts/STS_flickr30k/```

__Recover__ predicted similarity. Run the following script to join predictions
with input file. You can join predictions for more than one model by specifying
it in a __configuration file__:

```
   data_source=../STS_coco/source/coco.pairs.token.tsv
   systems=overlap,caverage,image,ml

   output=../STS_coco/coco.pairs.token.preds.tsv

   overlap=../STS_coco/bow/prediction.overlap.txt \
   caverage=../STS_coco/bow/prediction.caverage.txt \
   image=../STS_coco/image/prediction.resnet50.txt \
   ml=..sts/STS_coco/ml/test.predictions.txt \
```

In order to get all the predictions for each dataset (img-sim and rnd) you can run the following python scripts:
   
```
   [shell]% cd sts/postprocess
   [shell]% python join_predictions.py --config_file coco.img-sim.config.file
   [shell]% python join_predictions.py --config_file coco.rnd.config.file

   [shell]% python join_predictions.py --config_file flickr30k.img-sim.config.file
   [shell]% python join_predictions.py --config_file flickr30k.rnd.config.file
```

From this we'll produce 4 files:
   
   - ```sts/STS_coco/coco.pairs.img-sim.token.preds.tsv```
   - ```sts/STS_coco/coco.pairs.token.preds.tsv```
   - ```sts/STS_flickr30k/flickr30k.pairs.img-sim.token.preds.tsv```
   - ```sts/STS_flickr30k/flickr30k.pairs.token.preds.tsv```

Finally, we could join files of each sampling mode easily with pandas, and store them in:
   
   - ```datasets/flickr30k+coco.pairs.img-sim.token.tsv```
   - ```datasets/flickr30k+coco.pairs.token.tsv```

----

### Sampling AMT examples
For the last selection we defined five range of similarity from 0 to 5
and randomly sample the same amount of pairs from  ```datasets/flickr30k+coco.pairs.token.tsv```
(_random sampling_) and ```datasets/flickr30k+coco.pairs.img-sim.token.tsv```
(_image similarity_) dataset. We set a sampling of maximum 1500 instances
from each file (i.e 300 instances per range).

```
   [shell]% cd sampling
   [shell]% ./generate_amt_sample.sh
```

Final sampled examples will be stored in ```datasets/flickr30k+coco.pairs.rnd+sim.sample.tsv```