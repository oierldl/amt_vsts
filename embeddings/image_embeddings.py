from __future__ import print_function

import sys
from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras.models import Model
from os import listdir
from os.path import isfile, join, basename
import numpy as np
import argparse


class NN_model(object):
    def __init__(self, model_name="resnet50"):
        self.model = None
        
        #load model
        if model_name == "resnet50":
            #base_model = ResNet50(weights='imagenet')
            #self.model = Model(input=base_model.input, output=base_model.get_layer('flatten_1').output)
            self.model = ResNet50(weights='imagenet', include_top=False, pooling='max')
        #if model_name == "vgg19":
        #    self.model = VGG19(weights='imagenet', include_top=False)
        else:
            print("That model is not implemented\n")
            sys.exit(0)

    def get_image_embedding(self, img):
        emb = self.model.predict(img)
        return emb
    
    def show_model(self):
        print (self.model.summary())
    

class Reader(object):
    def __init__(self, image_size=(224, 224)):
        self.target_size = image_size

    def get_images_files(self, path):
        files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
        return files
    
    def load_image(self, img_path):
        img = image.load_img(img_path, target_size=self.target_size)
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        return x

        
def main(feed_dict):
    input_dir = feed_dict["input_dir"]
    output_file = feed_dict["output_file"]
    model_name = feed_dict["model_name"]
    source = feed_dict["source"]
    
    model = NN_model(model_name)
    reader = Reader()
    ifiles = reader.get_images_files(input_dir)

    fo = open(output_file, "w")
    for ifile in ifiles:
        iid = basename(ifile)
        img = reader.load_image(ifile) 
        emb = model.get_image_embedding(img)
        if source == "":
            fo.write(ifile + " " + " ".join(emb.flatten().astype('str')) + "\n")
        else:
            fo.write(source+"/"+iid + " " + " ".join(emb.flatten().astype('str')) + "\n")
    fo.close()
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="KERAS based script for extracting image representations")

    parser.add_argument("--input_dir", type=str,
                        help="Directory path containing the images")
    parser.add_argument("--output_file", type=str,
                        help="Out put path for storing the representations")
       
    parser.add_argument("--model_name", default="resnet50", type=str,
                        help="Name of the pre-trained model: [resnet50, vgg16, vgg19, InceptionV3] (only resnet50 is ready))")
    parser.add_argument("--source", default="", type=str,
                        help="Prefix name of the image id. E.g.: flickr30k/4062894483.jpg")
    
    parser.add_argument("--show_model", action="store_true",
                        help="Show model's layers (names and dimensionalities)")

    arguments = parser.parse_args()

    if arguments.show_model:
        model = NN_model(arguments.model_name)
        model.show_model()
    else:
       feed_dict = { arg: value for arg, value in arguments.__dict__.items()}

       for arg in feed_dict:
           print("{}: {}".format(arg, feed_dict[arg]))
       print()
       
       main(feed_dict)
