#!/bin/bash

bin=.
coco_dir=../datasets/coco
images_dir=${coco_dir}/images
embedding_dir=${coco_dir}/emebddings


mkdir -P ${embedding_dir}

python ${bin}/image_embeddings.py --input_dir ${images_dir}/train2014 \
       --output_file ${embedding_dir}/coco.train2014.resnet50.tsv \
       --model_name "resnet50" \
       --source "coco"

python ${bin}/image_embeddings.py --input_dir ${images_dir}/val2014 \
       --output_file ${embedding_dir}/coco.val2014.resnet50.tsv \
       --model_name "resnet50" \
       --source "coco"

cat ${embedding_dir}/coco.train2014.resnet50.tsv ${embedding_dir}/coco.val2014.resnet50.tsv > ${embedding_dir}/coco.resnet50.tsv 
