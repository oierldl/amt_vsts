#!/bin/bash

flickr30_dir=../datasets/Flickr30kEntities
images_dir=${flickr30k_dir}/flickr30k-images
embedding_dir=${flickr30k_dir}/emebddings


mkdir -P ${embedding_dir}

python image_embeddings.py --input_dir ${images_dir} \
       --output_file ${embedding_dir}/flickr30k.resnet50.tsv \
       --model_name "resnet50" \
       --source "flickr30k"

