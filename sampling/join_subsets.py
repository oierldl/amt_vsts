import argparse
import pandas as pd
from utils import data_reader as dr


def main():
    parser = argparse.ArgumentParser(
        description="Merges two tsv files\n")
    parser.add_argument('file1', type=str, help="Input file 1")
    parser.add_argument('file2', type=str, help="Input file 2")
    parser.add_argument('ofile', type=str, help="Output file")
    parser.add_argument('--f1name', type=str, default="subset1", help="name for the first subset")
    parser.add_argument('--f2name', type=str, default="subset2", help="name for the second subset")

    args = parser.parse_args()

    data1 = dr.load_tsv(args.file1)
    print('file1 shape {}'.format(data1.shape))
    data2 = dr.load_tsv(args.file2)
    print('file2 shape {}'.format(data2.shape))

    data1['subset'] = args.f1name
    data2['subset'] = args.f2name

    conc = pd.concat([data1, data2])
    print('joined shape {}'.format(conc.shape))
    dr.write_data(conc, args.ofile)


if __name__ == "__main__":
    main()
