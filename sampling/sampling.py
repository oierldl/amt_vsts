import argparse
import pandas as pd


def load_data(fname):
    df = pd.read_csv(fname, sep="\t")
    df.loc[df['ml'] < 0, 'ml'] = 0.001
    return df


def write_data(df, fname=None):
    df.to_csv(fname, sep='\t', index=False)


def make_groups(df, bin_list=[(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)], sim_model='ml'):
    bins = pd.IntervalIndex.from_tuples(bin_list)
    df['sim_range'] = pd.cut(df[sim_model], bins)
    return df


def sample(df, props, total_nb=800, seed=1221):
    sampled_by_group = []
    i = 0
    for (sim_range, group) in df.groupby('sim_range'):
        print("{} shape={}".format(sim_range, group.shape))
        nb = int(props[i] * total_nb)
        if nb > group.shape[0]:
            print('\tSampled: {}'.format(group.shape[0]))
            sampled_by_group.append(group)
        else:
            nb = int(props[i] * total_nb)
            print('\tSampled: {}'.format(nb))
            sampled_by_group.append(group.sample(nb, random_state=seed))
        i += 1
    sampled = pd.concat(sampled_by_group)
    return sampled


def parse_ranges(string):
    rngs = []
    prs = []
    groups = string.split(';')
    for group in groups:
        rng, pr = group.split(":")
        prs.append(float(pr))
        rngs.append(tuple([int(i) for i in rng.split('-')]))
    return rngs, prs


def main():
    parser = argparse.ArgumentParser(
        description="Stratified sampling according to some similarity values\n")

    parser.add_argument('--input_file', type=str, default="/home/oier/Proiektuak/muster/amt_vsts/flickr30k.pairs.img-sim.tokens.tsv",
                        help="Input file with pairs and similarity")
    parser.add_argument('--output_file', type=str, default='./nopar.2000.sample.vsts.tsv')

    parser.add_argument('--nb_examples', type=int, default=2000,
                        help="Total amount of example to sample")
    parser.add_argument('--group_rates', type=str, default="0-1:0.2;1-2:0.2;2-3:0.2;3-4:0.2;4-5:0.2",
                        help="Similarity to range to group, and proportion for each group [default:0-1:0.2;1-2:0.2;2-3:0.2;3-4:0.2;4-5:0.2]")
    parser.add_argument('--model_name', type=str, default='ml',
                        help="Name of the model for taking the similarities")

    parser.add_argument("--seed", default=1221, type=str, help="Seed for the random generator")
    args = parser.parse_args()

    ranges, props = parse_ranges(args.group_rates)

    data = load_data(args.input_file)
    data = make_groups(data, bin_list=ranges, sim_model=args.model_name)
    sampled_data = sample(data, props=props, total_nb=args.nb_examples, seed=args.seed)
    write_data(sampled_data, fname=args.output_file)


if __name__ == '__main__':
    main()
