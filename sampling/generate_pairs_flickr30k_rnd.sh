#!/bin/bash

flickr30k_dir=../datasets/Flickr30kEntities

python generate_pairs.py --dataset ${flickr30k_dir}/results_20130124.token \
       --outfile ${flickr30k_dir}/flickr30k.pairs.token.tsv \
       --id_name "flickr30" \
       --img_prefix "images/flickr30k" \
       --folds 5 \
       --verbose
