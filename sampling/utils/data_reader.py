import pandas as pd
import numpy as np


def load_images(fname):
    f = open(fname)
    images = [im.strip() for im in f]
    return images


def load_texts(basename, nfolds=5):
    texts = []
    for i in range(1, nfolds+1):
        texts.append([])
        f = open(basename + "." + str(i), "r")
        for text in f:
            texts[i-1].append(text.strip())
        f.close()
    return texts


def load_data(fname):
    image2text = {}
    f = open(fname, 'r')
    for line in f.readlines():
        image, text = line.rstrip().split('\t')
        image, fold = image.split('#')
        if image not in image2text:
            image2text[image] = []
        image2text[image].append(text)
    f.close()
    return image2text


def load_embeddings(fname):
    embeddings = []
    items = []
    # read all embeddings
    f = open(fname, "r")
    for emb_line in f:
        emb = emb_line.split(" ")
        items.append(emb[0])
        embeddings.append(np.array(emb[1:], dtype=float))
    f.close()

    # convert to nunpy arrays
    emb_size = len(embeddings[0])
    count = len(embeddings)
    matrix = np.empty((count, emb_size))
    for i in range(count):
        matrix[i] = embeddings[i]

    print('Found {} vectors of size {}'.format(count, emb_size))
    return items, matrix


def load_tsv(fname):
    df = pd.read_csv(fname, sep="\t")
    return df


def write_data(df, fname=None):
    df.to_csv(fname, sep='\t', index=False)
