from __future__ import print_function

import os.path
import numpy as np
from nltk import word_tokenize
import string
from . import data_reader as dr

import time


def overlap(sent1, sent2):
    def tokenize(string):
        return word_tokenize(string)

    def remove_punctuation(tokens):
        return [word for word in tokens if not all([c in string.punctuation for c in word])]

    def vocabulary(sents):
        vocab = set()
        for sent in sents:
            for w in sent:
                vocab.add(w)
        return vocab

    sent1 = tokenize(sent1)
    sent1 = remove_punctuation(sent1)
    sent2 = tokenize(sent2)
    sent2 = remove_punctuation(sent2)

    vocab = vocabulary([sent1, sent2])
    count = 0
    for w in sent1:
        if w in sent2:
            count += 1
    return float(count) / len(vocab)


def generate_dataset(image_f, text_bn, n=1000, folds=5):
    # Load data
    images = dr.load_images(image_f)
    texts = dr.load_texts(text_bn, folds)

    def choose_pairs(i, j):
        texti = texts[np.random.randint(folds)][i]
        textj = texts[np.random.randint(folds)][j]
        return texti + '\t' + images[i] + '\t' + textj + '\t' + images[j]

    # Generate examples index
    pairs = []
    indeces = np.random.choice(len(images), n, replace=False)
    for i in range(0, len(indeces)):
        for j in range(i+1, len(indeces)):
            pairs.append(choose_pairs(i, j))

    return pairs


def find_first_available(nn, used):
    for i in range(nn.shape[0]):
        if nn[i] not in used:
            return nn[i]
    return None


def get_similar_captions(captions1, captions2):
    max_sim = -1
    caption1 = None
    caption2 = None
    for sent1 in captions1:
        for sent2 in captions2:
            sim = overlap(sent1, sent2)
            if sim > max_sim:
                max_sim = sim
                caption1 = sent1
                caption2 = sent2
    return caption1, caption2, max_sim


def generate_dataset_no_repeated_images(data_f, folds=5, sim_captions=False, idname='flickr30k', prefix="images/flickr30k/"):
    image2texts = dr.load_data(data_f)
    images = list(image2texts.keys())

    # Sample image pairs
    pairs = []
    ii = 0

    while len(images) > 1:
        image1 = images[0]
        i = np.random.randint(1, len(images))
        image2 = images[i]

        # Get captions
        if sim_captions:
            text1, text2, _ = get_similar_captions(image2texts[image1], image2texts[image2])
        else:
            text1 = image2texts[image1][np.random.randint(folds)]
            text2 = image2texts[image2][np.random.randint(folds)]

        del images[i]
        del images[0]

        pairs.append(idname + '-' + str(ii) +'\t' + idname + '\t'+text1 + '\t' + prefix+"/"+image1 + '\t'
                    + text2 + '\t' + prefix+"/"+image2)
        ii += 1
    return pairs


def generate_dataset_similar_and_no_repeated_images(data_f, img_embs_f, folds=5,
                                                    sim_captions=False,
                                                    idname='flickr30k', prefix="images/flickr30k",
                                                    verbose=False):
    # display process every 100 loops
    display = 100
    if verbose:
        print("Generating pairs: ", end='\n')
    
    image2texts = dr.load_data(data_f)
    images = list(image2texts.keys())

    index2img, matrix = dr.load_embeddings(img_embs_f)
    img2index = {img:i for i, img in enumerate(index2img)}

    # Sample image pairs
    pairs = []
    ii = 0
    used = set()
    tt = time.time()
    for img_i in range(len(images) - 1):
        
        image1 = images[img_i]
        i = img2index[idname+"/"+image1]

        if i in used:
            continue

        if verbose and ii == 0:
            t1 = time.time()

        # similarities
        sim = matrix[i].dot(matrix.T)
        if verbose and ii==0:
            t2 = time.time()
            print('time for computing similarities: {}'.format(t2 - t1))
            
        # sorting
        knn = np.argsort(-sim)
        if verbose and ii==0:
            t3 = time.time()
            print('time for sorting similarities: {}'.format(t3 - t2))
        
        used.add(i)
        
        t1 = time.time()
        j = find_first_available(knn, used)
        if verbose and ii == 0:
            print('time for find first available: {}'.format(time.time()-t1))
        
        #print('{}: {} - {}'.format(ii, i, j))
        # if not any image available finish
        if j is None:
            print("Not available. Number of pairs: {}".format(len(pairs)))
            return pairs

        image2 = os.path.basename(index2img[j])

        if sim_captions:
            text1, text2, _ = get_similar_captions(image2texts[image1], image2texts[image2])
        else:
            text1 = image2texts[image1][np.random.randint(folds)]
            text2 = image2texts[image2][np.random.randint(folds)]

        # remove images from matrix
        used.add(j)

        pairs.append(idname + '-' + str(ii) +'\t' + idname + '\t'+text1 + '\t' + prefix+'/'+image1 + '\t'+
                     text2 + '\t' + prefix+'/'+image2)
        ii += 1
        if verbose and ii % display == 0:
            print('time for computing {} loops (ite={}): {}'.format(display, ii, time.time() - tt))
            tt = time.time()
    return pairs


def dump_pairs(pairs, out_f, header=None):
    # Write generated dataset
    f = open(out_f, 'w')
    if header is not None:
        f.write(header+'\n')
    for p in pairs:
        f.write(p + '\n')
    f.close()
