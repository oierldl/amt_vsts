#!/bin/bash

## INPUT
# file containing pairs of random images (more likely to be very different images)
in_rnd_img_file=../datasets/flickr30k+coco.pairs.token.preds.tsv
name1="rnd"

# file containing pairs of similar images
in_sim_img_file=../datasets/flickr30k+coco.pairs.img-sim.token.preds.tsv
name2="sim"

## OUTPUT (when ready to use on amazon turk, change name to amt-sample
out_sim_img_file=../datasets/flickr30k+coco.pairs.img-sim.sample.tsv
out_rnd_img_file=../datasets/flickr30k+coco.pairs.token.sample.tsv

## AMT RESULT
amt_file=../datasets/flickr30k+coco.pairs.rnd+sim.sample.tsv

## NUMBER of examples per subset
nb_example="1500"
model_name="ml"
group_rates="0-1:0.2;1-2:0.2;2-3:0.2;3-4:0.2;4-5:0.2"

python sampling.py --input_file ${in_rnd_img_file} \
                   --output_file ${out_rnd_img_file} \
                   --nb_example ${nb_example} \
                   --group_rates ${group_rates} \
                   --model_name ${model_name}

python sampling.py --input_file ${in_sim_img_file} \
                   --output_file ${out_sim_img_file} \
                   --nb_example ${nb_example} \
                   --group_rates ${group_rates} \
                   --model_name ${model_name}

python join_subsets.py ${out_rnd_img_file} ${out_sim_img_file} ${amt_file} --f1name ${name1} --f2name ${name2}