#!/bin/bash

flickr30k_dir=../datasets/Flickr30kEntities
embedding_file=../embeddings/flickr30k.resnet50.tsv

python generate_pairs.py --dataset ${flickr30k_dir}/results_20130124.token \
       --outfile ${flickr30k_dir}/flickr30k.pairs.token.img-sim.tsv \
       --img_sim --img_embs_file ${embedding_file} \
       --id_name "flickr30" \
       --img_prefix "images/flickr30k" \
       --folds 5 \
       --verbose
