#!/bin/bash

coco_dir=/datasets/coco/annotations
embedding_file=../embeddings/coco.resnet50.tsv

python generate_pairs.py --dataset ${coco_dir}/coco_captions.token.tsv \
       --outfile ${coco_dir}/coco.pairs.token.img-sim.tsv \
       --img_sim --img_embs_file ${embedding_file} \
       --id_name "coco" \
       --img_prefix "images/coco" \
       --folds 5 \
       --verbose
