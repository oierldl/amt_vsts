import sys
import os
import argparse
import numpy as np

#Add PYTHONPATH up to project's main directory
project_dir = os.path.realpath(__file__+'/..')
sys.path.append(project_dir)

from utils import generate_dataset as gd


def main():
    parser = argparse.ArgumentParser(
        description="Tool for extracting sentence pairs with the images and similarity values\n")

    parser.add_argument("--dataset", default=None, type=str,
                        help="Dataset file of image ids and captions")
    parser.add_argument('--outfile', default=None, type=str,
                        help="Output file name")

    parser.add_argument("--cap_sim", action="store_true",
                        help="Pair captions that maximize text overlap")

    parser.add_argument("--img_sim", action="store_true",
                        help="Pair image with its most similar image (needs image embeddings)")
    parser.add_argument('--img_embs_file', default=None, type=str,
                        help="Image embeddings file (used only with --img_sim)")

    parser.add_argument("--id_name", default="flickr30k", type=str,
                        help="meta information of the source of the image")
    parser.add_argument("--img_prefix", default="image/flickr30k", type=str,
                        help="Prefix of the image path")
    parser.add_argument("--folds", default=5, type=int,
                        help="Number of captions per image")

    parser.add_argument("--verbose", action="store_true",
                        help="prints for debugging")
    
    parser.add_argument("--seed", default=1221, type=str,
                        help="")
    args = parser.parse_args()

    if args.dataset is None or args.outfile is None:
        parser.print_help()
        parser.exit(message='missing arguments')

    if args.img_sim and args.img_embs_file is None:
        parser.print_help()
        parser.exit(message='missing image embeddings file (--img_embs_file)')

    np.random.seed(args.seed)
    if args.img_sim:
        pairs = gd.generate_dataset_similar_and_no_repeated_images(args.dataset, args.img_embs_file,
                                                                   folds=args.folds,
                                                                   sim_captions=args.cap_sim,
                                                                   idname=args.id_name,
                                                                   prefix=args.img_prefix, verbose=args.verbose)
    else:
        pairs = gd.generate_dataset_no_repeated_images(args.dataset,
                                                       folds=args.folds,
                                                       sim_captions=args.cap_sim,
                                                       idname=args.id_name,
                                                       prefix=args.img_prefix)

    gd.dump_pairs(pairs, args.outfile, header="id\tsource\tsent1\timage1\tsent2\timage2")


if __name__ == '__main__':
    main()
