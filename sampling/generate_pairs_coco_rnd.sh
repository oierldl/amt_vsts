#!/bin/bash

coco_dir=/datasets/coco/annotations

python generate_pairs.py --dataset ${coco_dir}/coco_captions.token.tsv \
       --outfile ${coco_dir}/coco.pairs.token.tsv \
       --id_name "coco" \
       --img_prefix "images/coco" \
       --folds 5 \
       --verbose
