import argparse
import json
import nltk


def tokenize(string):
    tokens = nltk.word_tokenize(string)
    return ' '.join(tokens)


def main():
    parser = argparse.ArgumentParser(
        description="Script for extracting and preprocessing images and texts\n")

    parser.add_argument("--inputfile", default="../datasets/coco/annotations/captions_train2014.json", type=str,
                        help="Dataset file of image ids and captions")
    parser.add_argument('--outfile', default="../datasets/coco/annotations/coco_captions.train2014.token.tsv", type=str,
                        help="Output file name")
    parser.add_argument('--tokenized_input', action="store_true",
                        help="Indicates that input text is already tokenized") 
    args = parser.parse_args()

    with open(args.inputfile) as json_data:
        d = json.load(json_data)

    image_ids = {}
    for image in d['images']:
        image_ids[image['id']] = image['file_name']

    image_captions = {}
    for caption in d['annotations']:
        img_id = caption['image_id']
        if img_id not in image_captions:
            image_captions[img_id] = []
        if args.tokenized_input:
            image_captions[img_id].append(caption['caption'].strip())
        else:
            image_captions[img_id].append(tokenize(caption['caption'].strip()))

    f_out = open(args.outfile, 'w')
    for im_id in image_ids.keys():
        for i, caption in enumerate(image_captions[im_id]):
            f_out.write('{}#{}\t{}\n'.format(image_ids[im_id], i, caption))
    f_out.close()


if __name__ == '__main__':
    main()
